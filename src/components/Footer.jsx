import { coinmarketcap, email, github, linkedin, telegram, twitter } from '@/configs/icons'
import Image from 'next/image'
import React from 'react'

export default function footer() {
  return (
    <div className="py-9 bg-background-title-light text-center flex flex-col gap-9">
      <div className="container">
        <div>
          <div className="text-center font-bold text-xl lg:text-4xl">COME HELP MAKE @CRYPTO A BETTER PLACE</div>
          <a href="#" className="py-[14px] px-7 text-white font-semibold bg-primary hover:bg-[#24A6FF] transition-all duration-200 rounded-full mt-3 block w-fit mx-auto">Join Telelabs</a>
        </div>

        <div className="border-t border-t-[#e1e0e0] pt-5 mt-5 lg:mt-8 lg:pt-8  flex justify-between w-full">
          <div className="flex gap-1">
            <Image src="/images/logo.svg" width={32} height={32} alt="logo" className="w-6 md:w-8" />
            <span className="text-lg md:text-2xl font-bold">TELELABS</span>
          </div>

          <div className="flex gap-2">
            <a href="#" className="text-[#C0D1D9] transition-all duration-150 hover:text-primary">
              {linkedin()}
            </a>
            <a href="#" className="text-[#C0D1D9] transition-all duration-150 hover:text-primary">
              {telegram()}
            </a>
            <a href="#" className="text-[#C0D1D9] transition-all duration-150 hover:text-primary">
              {github()}
            </a>
            <a href="#" className="text-[#C0D1D9] transition-all duration-150 hover:text-primary">
              {twitter()}
            </a>
            <a href="#" className="text-[#C0D1D9] transition-all duration-150 hover:text-primary">
              {email()}
            </a>
            <a href="#" className="text-[#C0D1D9] transition-all duration-150 hover:text-primary">
              {coinmarketcap()}
            </a>
          </div>
        </div>
      </div>
    </div>
  )
}
