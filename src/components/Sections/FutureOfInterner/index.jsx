import Image from 'next/image'
import Link from 'next/link'
import React from 'react'

export default function index() {
    return (
        <section className="py-12 lg:py-24">
            <div className="container">
                <div className="lg:w-1/2">
                    <h2 className="text-2xl text-center lg:text-start lg:text-[40px] font-bold">Dive into the future of internet</h2>
                </div>
                <div className="mt-12 flex flex-col gap-4">
                    <div className="bg-gradient-3 px-5 py-6 lg:px-10 lg:py-12 rounded-2xl flex flex-col lg:flex-row">
                        <div className="lg:h-fit">
                            <div className="text-xs text-white bg-[hsla(0,0%,100%,.07)] w-fit rounded-full py-1 px-2">version 1.0</div>
                            <div className="text-xl font-semibold mt-1 text-white">TON Sites, TON Proxy, TON WWW</div>
                            <p className="block mt-2 text-white">Try the decentralized, secure, reliable Internet of the future with TON services.</p>

                            <div className="mt-2 flex gap-2">
                                <a href="#" className="px-4 py-2 block w-fit bg-white rounded-full font-bold text-sm text-primary">Read more</a>
                                <a href="#" className="px-4 py-2 w-fit rounded-full font-bold text-sm text-white flex items-center">
                                    Roadmap
                                    <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M8 18L13 13.5L8 9" stroke="currentColor" strokeWidth="2.2" strokeLinecap="round" strokeLinejoin="round" />
                                    </svg>
                                </a>
                            </div>
                        </div>
                        <Image src="/images/banner-1.png" width="496" height="180" className="" />
                    </div>
                    <div className="bg-gradient-4 px-5 py-6 rounded-2xl">
                        <div className="text-xs text-white bg-[hsla(0,0%,100%,.07)] w-fit rounded-full py-1 px-2">version 1.0</div>
                        <div className="text-xl font-semibold mt-1 text-white">TON Storage</div>
                        <p className="block mt-2 text-white">Keep your data safe and secure with the decentralized storage of the future.</p>

                        <div className="mt-2 flex gap-2">
                            <a href="#" className="px-4 py-2 block w-fit bg-white rounded-full font-bold text-sm text-primary">Read more</a>
                            <a href="#" className="px-4 py-2 w-fit rounded-full font-bold text-sm text-white flex items-center">
                                Roadmap
                                <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M8 18L13 13.5L8 9" stroke="currentColor" strokeWidth="2.2" strokeLinecap="round" strokeLinejoin="round" />
                                </svg>
                            </a>
                            <a href="#" className="px-4 py-2 w-fit rounded-full font-bold text-sm text-white flex items-center">
                                Dev Docs
                                <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M8 18L13 13.5L8 9" stroke="currentColor" strokeWidth="2.2" strokeLinecap="round" strokeLinejoin="round" />
                                </svg>
                            </a>
                        </div>
                        <Image src="/images/banner-2.png" width="496" height="180" className="-mb-6" />
                    </div>
                </div>
            </div>
        </section>
    )
}
