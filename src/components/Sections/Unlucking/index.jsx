import Image from 'next/image'
import Link from 'next/link'
import React from 'react'

export default function index() {
    return (
        <section className="py-16 lg:py-24">
            <div className="container">
                <div className="lg:w-1/2">
                    <h2 className="text-3xl md:text-[40px] font-bold text-center md:text-start">Unlocking freedom with Decentralized Products.</h2>
                </div>
                <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-8 mt-12">
                    <a href="#" className="flex flex-col justify-between px-7 py-8 bg-[#F0F4F8] rounded-2xl transition-all duration-300 hover:shadow-md hover:bg-background-title-light">
                        <Image src="/images/icons/warning.png" width="14" height="14" alt="warning" />
                        <div className="flex flex-col gap-2 mt-14">
                            <span className="font-semibold text-2xl">@CRYPTO</span>
                            <p className="text-lg">Funnel to @crypto</p>
                            <p className="font-semibold text-lg text-primary flex gap-1 items-center">Explore options
                                <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M8 18L13 13.5L8 9" stroke="currentColor" strokeWidth="2.2" strokeLinecap="round" strokeLinejoin="round" />
                                </svg>
                            </p>
                        </div>
                    </a>
                    <a href="#" className="flex flex-col justify-between px-7 py-8 bg-[#F0F4F8] rounded-2xl transition-all duration-300 hover:shadow-md hover:bg-background-title-light">
                        <Image src="/images/icons/warning.png" width="14" height="14" alt="warning" />
                        <div className="flex flex-col gap-2 mt-14">
                            <span className="font-semibold text-2xl">@TRANSACT BOT</span>
                            <p className="text-lg">@transact any @crypto to $TON, instantly</p>
                            <p className="font-semibold text-lg text-primary flex gap-1 items-center">Explore options
                                <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M8 18L13 13.5L8 9" stroke="currentColor" strokeWidth="2.2" strokeLinecap="round" strokeLinejoin="round" />
                                </svg>
                            </p>
                        </div>
                    </a>
                    <a href="#" className="flex flex-col justify-between px-7 py-8 bg-[#F0F4F8] rounded-2xl transition-all duration-300 hover:shadow-md hover:bg-background-title-light">
                        <Image src="/images/icons/warning.png" width="14" height="14" alt="warning" />
                        <div className="flex flex-col gap-2 mt-14">
                            <span className="font-semibold text-2xl">@STREAM</span>
                            <p className="text-lg">@stream any $TON @games live</p>
                            <p className="font-semibold text-lg text-primary flex gap-1 items-center">Explore options
                                <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M8 18L13 13.5L8 9" stroke="currentColor" strokeWidth="2.2" strokeLinecap="round" strokeLinejoin="round" />
                                </svg>
                            </p>
                        </div>
                    </a>
                    <a href="#" className="flex flex-col justify-between px-7 py-8 bg-[#F0F4F8] rounded-2xl transition-all duration-300 hover:shadow-md hover:bg-background-title-light">
                        <Image src="/images/icons/warning.png" width="14" height="14" alt="warning" />
                        <div className="flex flex-col gap-2 mt-14">
                            <span className="font-semibold text-2xl">@MINT BOT</span>
                            <p className="text-lg">1 click @mint any @crypto in $TON</p>
                            <p className="font-semibold text-lg text-primary flex gap-1 items-center">Explore options
                                <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M8 18L13 13.5L8 9" stroke="currentColor" strokeWidth="2.2" strokeLinecap="round" strokeLinejoin="round" />
                                </svg>
                            </p>
                        </div>
                    </a>
                    <a href="#" className="flex flex-col justify-between px-7 py-8 bg-[#F0F4F8] rounded-2xl transition-all duration-300 hover:shadow-md hover:bg-background-title-light">
                        <Image src="/images/icons/warning.png" width="14" height="14" alt="warning" />
                        <div className="flex flex-col gap-2 mt-14">
                            <span className="font-semibold text-2xl">@GAMES BOT</span>
                            <p className="text-lg">Play any @crypto @games in the $TON ecosystem</p>
                            <p className="font-semibold text-lg text-primary flex gap-1 items-center">Explore options
                                <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M8 18L13 13.5L8 9" stroke="currentColor" strokeWidth="2.2" strokeLinecap="round" strokeLinejoin="round" />
                                </svg>
                            </p>
                        </div>
                    </a>
                    <a href="#" className="flex flex-col justify-between px-7 py-8 bg-[#F0F4F8] rounded-2xl transition-all duration-300 hover:shadow-md hover:bg-background-title-light">
                        <Image src="/images/icons/warning.png" width="14" height="14" alt="warning" />
                        <div className="flex flex-col gap-2 mt-14">
                            <span className="font-semibold text-2xl">@SCAN BOT</span>
                            <p className="text-lg">@scan any token on any chain, instantly</p>
                            <p className="font-semibold text-lg text-primary flex gap-1 items-center">Explore options
                                <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M8 18L13 13.5L8 9" stroke="currentColor" strokeWidth="2.2" strokeLinecap="round" strokeLinejoin="round" />
                                </svg>
                            </p>
                        </div>
                    </a>
                </div>
                <Link className="py-[14px] px-7 text-white bg-primary rounded-full font-semibold block w-fit mt-14 shadow-xl mx-auto lg:hover:scale-95 transition-all duration-150" href="/products">Explore more Products</Link>
            </div>
        </section>
    )
}
