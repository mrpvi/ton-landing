import { dns, mobile, tribute } from '@/configs/icons'
import Image from 'next/image'
import Link from 'next/link'
import React from 'react'

export default function index() {
    return (
        <section className="py-16 lg:py-24">
            <div className="container">
                <div className="flex items-center justify-between">
                    <h2 className="text-3xl md:text-[40px] font-bold text-center md:text-start">Explore apps and services</h2>
                    <p className="font-bold text-lg text-primary lg:flex gap-1 items-center hidden">
                        Explore all ecosystem
                        <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M8 18L13 13.5L8 9" stroke="currentColor" strokeWidth="2.2" strokeLinecap="round" strokeLinejoin="round" />
                        </svg>
                    </p>
                </div>
                <div className="grid grid-cols-1 lg:grid-cols-3 gap-8 mt-12">
                    <a href="#" className="bg-[rgba(7,_172,_255,_1)] hover:bg-[rgba(45,_185,_255,_1)] transition-all p-7 pb-0 rounded-3xl shadow-[0_2px_24px_0_rgba(114,138,150,.12)] flex flex-col relative">
                        <div className="font-semibold">
                            {mobile()}
                        </div>
                        <div className="text-xl text-white font-semibold mt-1">Anonymous eSIM</div>
                        <p className="mt-1 leading-5 text-white">Affordable and secure mobile internet provider for home & travel.</p>
                        <Image src="/images/shot/ton-mobile.png" width="247" height="384" className="mt-7 mx-auto" alt="shot" />
                    </a>
                    <a href="#" className="bg-[#1F2733] transition-all p-7 pb-0 rounded-3xl shadow-[0_2px_24px_0_rgba(114,138,150,.12)] flex flex-col relative">
                        <div className="font-semibold">
                            {dns()}
                        </div>
                        <div className="text-xl text-white font-semibold mt-1">Easy-to-remember address</div>
                        <p className="mt-1 leading-5 text-white">Your wallet or website can be easily found in the TON network.</p>
                        <Image src="/images/shot/domains.png" width="247" height="384" className="mt-7 mx-auto" alt="shot" />
                    </a>
                    <a href="#" className="bg-gradient-1 hover:bg-gradient-2 transition-all p-7 pb-0 rounded-3xl shadow-[0_2px_24px_0_rgba(114,138,150,.12)] flex flex-col relative">
                        <div className="font-semibold">
                            {tribute()}
                        </div>
                        <div className="text-xl text-white font-semibold mt-1">Monetize your audience</div>
                        <p className="mt-1 leading-5 text-white">Earn from subscriptions and donations to your community.</p>
                        <Image src="/images/shot/tribute.png" width="247" height="384" className="mt-7 mx-auto" alt="shot" />
                    </a>
                </div>
            </div>
        </section>
    )
}
