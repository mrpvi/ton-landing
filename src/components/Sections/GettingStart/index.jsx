import Image from 'next/image'
import Link from 'next/link'
import React from 'react'

export default function index() {
    return (
        <section className="bg-background-title-light py-16 lg:py-24">
            <div className="container">
                <div className="flex justify-between items-end">
                    <span className="font-bold text-[1.4rem] text-center mx-auto md:text-start md:mx-0 lg:text-5xl">
                        Getting started is easy:<br></br>
                        <div className="flex items-center">
                            all you need is
                            <Image src="/images/icons/telegram.svg" width="40" height="40" alt="telegram" className="lg:w-[64px]" />
                            <div className="gradient-text leading-[1.2]">Telegram</div>
                        </div>
                    </span>

                    <Link className="py-[14px] px-7 text-white bg-primary hover:bg-[#24A6FF] transition-all duration-200 rounded-full font-semibold hidden md:block" href="#">Get Wallet</Link>
                </div>

            </div>
            <div className="overflow-x-auto overflow-y-hidden mt-12 no-scrollbar">
                <div className="container flex gap-7">
                    <div className="bg-[#26ACFF] p-6 rounded-3xl shadow-[0_2px_24px_0_rgba(114,138,150,.12)] min-w-[352px] h-[522px] flex flex-col justify-between relative">
                        <div className="text-white font-semibold text-xl">
                            <Image src="/images/icons/cardWalletLogo.svg" width="22" height="18" alt="shot" className="me-1 inline-block" />
                            Wallet gives direct and easy access to TON from Telegram
                        </div>
                        <Image src="/images/shot/card0.png" width="247" height="384" className="-mb-6 mx-auto" alt="shot" />
                    </div>
                    <div className="bg-white p-6 rounded-3xl shadow-[0_2px_24px_0_rgba(114,138,150,.12)] min-w-[352px] h-[522px] flex flex-col justify-between relative">
                        <Image src="/images/shot/card1_1.png" width="247" height="384" className="-mt-7 mx-auto" alt="shot" />
                        <div className="text-black font-semibold text-xl">
                            Enjoy <span className="text-primary">commission-free</span> crypto transfers to any Telegram user
                        </div>
                    </div>
                    <div className="bg-[#1F2337] p-6 rounded-3xl shadow-[0_2px_24px_0_rgba(114,138,150,.12)] min-w-[352px] h-[522px] flex flex-col justify-between relative">
                        <div className="text-white font-semibold text-xl">
                            Retain <span className="text-primary">full control</span> of your Toncoin and other digital assets with TON Space
                        </div>
                        <Image src="/images/shot/card2.png" width="247" height="384" className="-mb-6 mx-auto" alt="shot" />
                    </div>
                    <div className="bg-white p-6 rounded-3xl shadow-[0_2px_24px_0_rgba(114,138,150,.12)] min-w-[352px] h-[522px] flex flex-col justify-between relative">
                        <Image src="/images/shot/card3.png" width="247" height="384" className="-mt-7 mx-auto" alt="shot" />
                        <div className="text-black font-semibold text-xl">
                            <div className="text-primary">Seamlessly interact</div>
                            with web3 apps
                        </div>
                    </div>
                </div>

            </div>
            <Link className="py-[14px] px-7 text-white bg-primary rounded-full font-semibold md:hidden block w-fit mx-auto mt-8" href="#">Get Wallet</Link>
        </section>
    )
}
