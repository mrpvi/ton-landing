'use client'
import Link from 'next/link'
import React from 'react'

export default function index() {
    return (
        <section className="py-16 lg:py-32">
            <div className="container flex flex-col lg:flex-row gap-12">
                <div className="lg:w-1/2 flex flex-col justify-between">
                    <div>
                        <h2 className="md:text-[40px] text-3xl font-bold text-center lg:text-start">TON is the heart of Telelabs</h2>
                        <p className="mt-1 text-center lg:text-start lg:mt-3">Toncoin, the native cryptocurrency of TON, facilitates network operations, @transactions, and supports dApps, including @games and @collectibles , on the Telelabs platform.</p>
                    </div>
                    <Link className="py-[14px] px-7 text-white bg-primary rounded-full font-semibold block w-fit mt-4 mx-auto lg:mx-0" href="#">Get Toncoin</Link>
                </div>
                <div className="lg:w-1/2 rounded-xl">
                    <script type="text/javascript" src="https://files.coinmarketcap.com/static/widget/currency.js"></script>
                    <div className="coinmarketcap-currency-widget bg-[#F7F9FB] border-none" data-currencyid="11419" data-base="USD" data-secondary="USD" data-ticker="true" data-rank="true" data-marketcap="true" data-volume="true" data-statsticker="true" data-stats="USD"></div>
                </div>
            </div>
        </section>
    )
}
