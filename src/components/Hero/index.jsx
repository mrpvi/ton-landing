'use client'
import Link from 'next/link'
import React from 'react'
import { TypeAnimation } from 'react-type-animation'

export default function index() {
  return (
    <section className="text-center py-10 lg:py-[132px] bg-white">
      <div className="container flex flex-col justify-center items-center gap-4">
        <h1 className="text-[53px] tracking-tight leading-[1] sm:text-6xl lg:text-[95px] font-bold">
          Changing WEB3 through
          <div className="mt-3 block">
            <TypeAnimation
              sequence={[
                'Telegram',
                3000,
                'Crypto',
                3000,
                'Community',
                3000,
                'Artificial Intelligence',
                3000,
                'NFTs',
                3000,
                'Gaming',
                3000,
                'Defi',
                3000,
                'VPN',
                3000,
                'Metaverse',
                3000,
                'Staking',
                3000,
                'Blockchain',
                3000,
                'Technology',
                3000,
                'Trading',
                3000,
                'Transactions',
                3000,
                'Tokens',
                3000,
              ]}
              speed={50}
              cursor={false}
              style={{
                width: '100%',
                display: 'block',
                lineHeight: '1.2',
              }}
              repeat={Infinity}
              className="gradient-text mx-auto after:text-primary min-h-[63.60px] lg:min-h-[114px]"
            />
          </div>
        </h1>
        <p className="max-w-[600px]">TON Coin is the core of Telelabs</p>
        <div className="flex flex-row gap-2 items-center justify-center text-sm md:text-base">
          <Link className="py-[14px] px-4 lg:px-7 border border-button-secondary-stroke-light rounded-full font-semibold" href="#">Join Crypto</Link>
          <Link className="py-[14px] px-4 lg:px-7 border border-button-secondary-stroke-light rounded-full font-semibold" href="#">Join Telelabs</Link>
        </div>
      </div>
    </section>
  )
}
