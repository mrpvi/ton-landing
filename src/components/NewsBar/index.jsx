import Image from 'next/image'
import React from 'react'

export default function index() {
    return (
        <div className="bg-news-gradient h-10 md:h-12">
            <div className="container  flex items-center justify-center h-full">
                <div className="w-fit cursor-pointer group flex items-center h-full">
                    <Image src="/images/icons/USDT.svg" width={28} height={28} alt="USDT" className="me-2 w-6 md:w-7" />
                    <span className="text-sm md:text-base text-nowrap text-white font-semibold leading-7">
                        Send Digital Dollars to anyone, anywhere
                    </span>
                    <div className="text-white">
                        <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" className="group-hover:animate-[transformRight_2s_ease-in-out_infinite]">
                            <path d="M8 18L13 13.5L8 9" stroke="currentColor" strokeWidth="2.2" strokeLinecap="round" strokeLinejoin="round" />
                        </svg>
                    </div>
                </div>
            </div>
        </div>
    )
}
