import Image from 'next/image'
import Link from 'next/link'
import React from 'react'

export default function index() {
    return (
        <div className="container">
            <div className="font-semibold flex justify-between items-center py-2 md:py-4">
                <div className="flex gap-1 items-center">
                    <Image src="/images/logo.svg" width={32} height={32} alt="logo" />
                    <span className="text-2xl font-bold">TELELABS</span>
                </div>
            </div>
        </div>
    )
}
