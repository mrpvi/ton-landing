import NewsBar from '@/components/NewsBar';
import Header from '@/components/Header';
import Hero from '@/components/Hero';
import GettingStart from '@/components/Sections/GettingStart';
import Info from '@/components/Sections/Info';
import Unlucking from '@/components/Sections/Unlucking';
import ExploreApps from '@/components/Sections/ExploreApps';
import FutureOfInterner from '@/components/Sections/FutureOfInterner';
import Footer from '@/components/Footer';
import Image from 'next/image';

export default function Products() {
  return (
    <main className="flex min-h-screen flex-col">
      <NewsBar />
      <Header />
      <section className="py-16 lg:py-24">
        <div className="container">
          <div className="">
            <h2 className="text-3xl md:text-[40px] font-bold text-center md:text-start">INFRASTRUCTURE IN DEVELOPMENT 2024</h2>
          </div>
          <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-8 mt-12">
            <a href="#" className="flex flex-col justify-between px-7 py-8 bg-[#F0F4F8] rounded-2xl">
              <Image src="/images/icons/warning.png" width="14" height="14" alt="warning" />
              <div className="flex flex-col gap-2 mt-14">
                <span className="font-semibold text-2xl">@CRYPTO</span>
                <p className="text-lg">Funnel to @crypto</p>
                <p className="font-semibold text-lg text-primary flex gap-1 items-center">Explore options
                  <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M8 18L13 13.5L8 9" stroke="currentColor" strokeWidth="2.2" strokeLinecap="round" strokeLinejoin="round" />
                  </svg>
                </p>
              </div>
            </a>
            <a href="#" className="flex flex-col justify-between px-7 py-8 bg-[#F0F4F8] rounded-2xl">
              <Image src="/images/icons/warning.png" width="14" height="14" alt="warning" />
              <div className="flex flex-col gap-2 mt-14">
                <span className="font-semibold text-2xl">@TRANSACT BOT</span>
                <p className="text-lg">@transact any @crypto to $TON, instantly</p>
                <p className="font-semibold text-lg text-primary flex gap-1 items-center">Explore options
                  <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M8 18L13 13.5L8 9" stroke="currentColor" strokeWidth="2.2" strokeLinecap="round" strokeLinejoin="round" />
                  </svg>
                </p>
              </div>
            </a>
            <a href="#" className="flex flex-col justify-between px-7 py-8 bg-[#F0F4F8] rounded-2xl">
              <Image src="/images/icons/warning.png" width="14" height="14" alt="warning" />
              <div className="flex flex-col gap-2 mt-14">
                <span className="font-semibold text-2xl">@STREAM</span>
                <p className="text-lg">@stream any $TON @games live</p>
                <p className="font-semibold text-lg text-primary flex gap-1 items-center">Explore options
                  <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M8 18L13 13.5L8 9" stroke="currentColor" strokeWidth="2.2" strokeLinecap="round" strokeLinejoin="round" />
                  </svg>
                </p>
              </div>
            </a>
            <a href="#" className="flex flex-col justify-between px-7 py-8 bg-[#F0F4F8] rounded-2xl">
              <Image src="/images/icons/warning.png" width="14" height="14" alt="warning" />
              <div className="flex flex-col gap-2 mt-14">
                <span className="font-semibold text-2xl">@MINT BOT</span>
                <p className="text-lg">1 click @mint any @crypto in $TON</p>
                <p className="font-semibold text-lg text-primary flex gap-1 items-center">Explore options
                  <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M8 18L13 13.5L8 9" stroke="currentColor" strokeWidth="2.2" strokeLinecap="round" strokeLinejoin="round" />
                  </svg>
                </p>
              </div>
            </a>
            <a href="#" className="flex flex-col justify-between px-7 py-8 bg-[#F0F4F8] rounded-2xl">
              <Image src="/images/icons/warning.png" width="14" height="14" alt="warning" />
              <div className="flex flex-col gap-2 mt-14">
                <span className="font-semibold text-2xl">@GAMES BOT</span>
                <p className="text-lg">Play any @crypto @games in the $TON ecosystem</p>
                <p className="font-semibold text-lg text-primary flex gap-1 items-center">Explore options
                  <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M8 18L13 13.5L8 9" stroke="currentColor" strokeWidth="2.2" strokeLinecap="round" strokeLinejoin="round" />
                  </svg>
                </p>
              </div>
            </a>
            <a href="#" className="flex flex-col justify-between px-7 py-8 bg-[#F0F4F8] rounded-2xl">
              <Image src="/images/icons/warning.png" width="14" height="14" alt="warning" />
              <div className="flex flex-col gap-2 mt-14">
                <span className="font-semibold text-2xl">@SCAN BOT</span>
                <p className="text-lg">@scan any token on any chain, instantly</p>
                <p className="font-semibold text-lg text-primary flex gap-1 items-center">Explore options
                  <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M8 18L13 13.5L8 9" stroke="currentColor" strokeWidth="2.2" strokeLinecap="round" strokeLinejoin="round" />
                  </svg>
                </p>
              </div>
            </a>
            <a href="#" className="flex flex-col justify-between px-7 py-8 bg-[#F0F4F8] rounded-2xl">
              <Image src="/images/icons/warning.png" width="14" height="14" alt="warning" />
              <div className="flex flex-col gap-2 mt-14">
                <span className="font-semibold text-2xl">@COMMUNITY</span>
                <p className="text-lg">@scan any token on any chain, instantly</p>
                <p className="font-semibold text-lg text-primary flex gap-1 items-center">Explore options
                  <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M8 18L13 13.5L8 9" stroke="currentColor" strokeWidth="2.2" strokeLinecap="round" strokeLinejoin="round" />
                  </svg>
                </p>
              </div>
            </a>
            <a href="#" className="flex flex-col justify-between px-7 py-8 bg-[#F0F4F8] rounded-2xl">
              <Image src="/images/icons/warning.png" width="14" height="14" alt="warning" />
              <div className="flex flex-col gap-2 mt-14">
                <span className="font-semibold text-2xl">@SCAN BOT</span>
                <p className="text-lg">Connect with the @crypto $TON @community</p>
                <p className="font-semibold text-lg text-primary flex gap-1 items-center">Explore options
                  <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M8 18L13 13.5L8 9" stroke="currentColor" strokeWidth="2.2" strokeLinecap="round" strokeLinejoin="round" />
                  </svg>
                </p>
              </div>
            </a>
            <a href="#" className="flex flex-col justify-between px-7 py-8 bg-[#F0F4F8] rounded-2xl">
              <Image src="/images/icons/warning.png" width="14" height="14" alt="warning" />
              <div className="flex flex-col gap-2 mt-14">
                <span className="font-semibold text-2xl">@SNIPER BOT</span>
                <p className="text-lg">Save 99% on all trades</p>
                <p className="font-semibold text-lg text-primary flex gap-1 items-center">Explore options
                  <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M8 18L13 13.5L8 9" stroke="currentColor" strokeWidth="2.2" strokeLinecap="round" strokeLinejoin="round" />
                  </svg>
                </p>
              </div>
            </a>
            <a href="#" className="flex flex-col justify-between px-7 py-8 bg-[#F0F4F8] rounded-2xl">
              <Image src="/images/icons/warning.png" width="14" height="14" alt="warning" />
              <div className="flex flex-col gap-2 mt-14">
                <span className="font-semibold text-2xl">@STOCKS BOT</span>
                <p className="text-lg">Live stock market data and tools</p>
                <p className="font-semibold text-lg text-primary flex gap-1 items-center">Explore options
                  <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M8 18L13 13.5L8 9" stroke="currentColor" strokeWidth="2.2" strokeLinecap="round" strokeLinejoin="round" />
                  </svg>
                </p>
              </div>
            </a>
            <a href="#" className="flex flex-col justify-between px-7 py-8 bg-[#F0F4F8] rounded-2xl">
              <Image src="/images/icons/warning.png" width="14" height="14" alt="warning" />
              <div className="flex flex-col gap-2 mt-14">
                <span className="font-semibold text-2xl">@ARTIFICIAL BOT</span>
                <p className="text-lg">Clone yourself with @artificial</p>
                <p className="font-semibold text-lg text-primary flex gap-1 items-center">Explore options
                  <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M8 18L13 13.5L8 9" stroke="currentColor" strokeWidth="2.2" strokeLinecap="round" strokeLinejoin="round" />
                  </svg>
                </p>
              </div>
            </a>
            <a href="#" className="flex flex-col justify-between px-7 py-8 bg-[#F0F4F8] rounded-2xl">
              <Image src="/images/icons/warning.png" width="14" height="14" alt="warning" />
              <div className="flex flex-col gap-2 mt-14">
                <span className="font-semibold text-2xl">@WELCOME BOT</span>
                <p className="text-lg">@welcome bot to the $TON ecosystem</p>
                <p className="font-semibold text-lg text-primary flex gap-1 items-center">Explore options
                  <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M8 18L13 13.5L8 9" stroke="currentColor" strokeWidth="2.2" strokeLinecap="round" strokeLinejoin="round" />
                  </svg>
                </p>
              </div>
            </a>
            <a href="#" className="flex flex-col justify-between px-7 py-8 bg-[#F0F4F8] rounded-2xl">
              <Image src="/images/icons/warning.png" width="14" height="14" alt="warning" />
              <div className="flex flex-col gap-2 mt-14">
                <span className="font-semibold text-2xl">@MEMECOINS</span>
                <p className="text-lg">@memecoins on the $TON blockchain</p>
                <p className="font-semibold text-lg text-primary flex gap-1 items-center">Explore options
                  <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M8 18L13 13.5L8 9" stroke="currentColor" strokeWidth="2.2" strokeLinecap="round" strokeLinejoin="round" />
                  </svg>
                </p>
              </div>
            </a>
            <a href="#" className="flex flex-col justify-between px-7 py-8 bg-[#F0F4F8] rounded-2xl">
              <Image src="/images/icons/warning.png" width="14" height="14" alt="warning" />
              <div className="flex flex-col gap-2 mt-14">
                <span className="font-semibold text-2xl">@CREATORS</span>
                <p className="text-lg">Connect with other Telegram / $TON @creators</p>
                <p className="font-semibold text-lg text-primary flex gap-1 items-center">Explore options
                  <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M8 18L13 13.5L8 9" stroke="currentColor" strokeWidth="2.2" strokeLinecap="round" strokeLinejoin="round" />
                  </svg>
                </p>
              </div>
            </a>
            <a href="#" className="flex flex-col justify-between px-7 py-8 bg-[#F0F4F8] rounded-2xl">
              <Image src="/images/icons/warning.png" width="14" height="14" alt="warning" />
              <div className="flex flex-col gap-2 mt-14">
                <span className="font-semibold text-2xl">@COLLECTIBLES BOT</span>
                <p className="text-lg">Secondary $TON NFT username marketplace</p>
                <p className="font-semibold text-lg text-primary flex gap-1 items-center">Explore options
                  <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M8 18L13 13.5L8 9" stroke="currentColor" strokeWidth="2.2" strokeLinecap="round" strokeLinejoin="round" />
                  </svg>
                </p>
              </div>
            </a>
            <a href="#" className="flex flex-col justify-between px-7 py-8 bg-[#F0F4F8] rounded-2xl">
              <Image src="/images/icons/warning.png" width="14" height="14" alt="warning" />
              <div className="flex flex-col gap-2 mt-14">
                <span className="font-semibold text-2xl">@FOUNDER</span>
                {/* <p className="text-lg">Connect with other Telegram / $TON @creators</p> */}
                <p className="font-semibold text-lg text-primary flex gap-1 items-center">Explore options
                  <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M8 18L13 13.5L8 9" stroke="currentColor" strokeWidth="2.2" strokeLinecap="round" strokeLinejoin="round" />
                  </svg>
                </p>
              </div>
            </a>
            <a href="#" className="flex flex-col justify-between px-7 py-8 bg-[#F0F4F8] rounded-2xl">
              <Image src="/images/icons/warning.png" width="14" height="14" alt="warning" />
              <div className="flex flex-col gap-2 mt-14">
                <span className="font-semibold text-2xl">@CALLERS</span>
                <p className="text-lg">Community of $TON @callers</p>
                <p className="font-semibold text-lg text-primary flex gap-1 items-center">Explore options
                  <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M8 18L13 13.5L8 9" stroke="currentColor" strokeWidth="2.2" strokeLinecap="round" strokeLinejoin="round" />
                  </svg>
                </p>
              </div>
            </a>
            <a href="#" className="flex flex-col justify-between px-7 py-8 bg-[#F0F4F8] rounded-2xl">
              <Image src="/images/icons/warning.png" width="14" height="14" alt="warning" />
              <div className="flex flex-col gap-2 mt-14">
                <span className="font-semibold text-2xl">@CRYPTOVPN</span>
                <p className="text-lg">Strong like bull VPN ($1 per month $TON)</p>
                <p className="font-semibold text-lg text-primary flex gap-1 items-center">Explore options
                  <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M8 18L13 13.5L8 9" stroke="currentColor" strokeWidth="2.2" strokeLinecap="round" strokeLinejoin="round" />
                  </svg>
                </p>
              </div>
            </a>
            <a href="#" className="flex flex-col justify-between px-7 py-8 bg-[#F0F4F8] rounded-2xl">
              <Image src="/images/icons/warning.png" width="14" height="14" alt="warning" />
              <div className="flex flex-col gap-2 mt-14">
                <span className="font-semibold text-2xl">@FIAT</span>
                <p className="text-lg">@fiat to $TON onramp/offramp</p>
                <p className="font-semibold text-lg text-primary flex gap-1 items-center">Explore options
                  <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M8 18L13 13.5L8 9" stroke="currentColor" strokeWidth="2.2" strokeLinecap="round" strokeLinejoin="round" />
                  </svg>
                </p>
              </div>
            </a>
            <a href="#" className="flex flex-col justify-between px-7 py-8 bg-[#F0F4F8] rounded-2xl">
              <Image src="/images/icons/warning.png" width="14" height="14" alt="warning" />
              <div className="flex flex-col gap-2 mt-14">
                <span className="font-semibold text-2xl">@FARM</span>
                <p className="text-lg">Notcoin $TON technology via @games</p>
                <p className="font-semibold text-lg text-primary flex gap-1 items-center">Explore options
                  <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M8 18L13 13.5L8 9" stroke="currentColor" strokeWidth="2.2" strokeLinecap="round" strokeLinejoin="round" />
                  </svg>
                </p>
              </div>
            </a>
            <a href="#" className="flex flex-col justify-between px-7 py-8 bg-[#F0F4F8] rounded-2xl">
              <Image src="/images/icons/warning.png" width="14" height="14" alt="warning" />
              <div className="flex flex-col gap-2 mt-14">
                <span className="font-semibold text-2xl">@KICK</span>
                <p className="text-lg">Gaming Community</p>
                <p className="font-semibold text-lg text-primary flex gap-1 items-center">Explore options
                  <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M8 18L13 13.5L8 9" stroke="currentColor" strokeWidth="2.2" strokeLinecap="round" strokeLinejoin="round" />
                  </svg>
                </p>
              </div>
            </a>
            <a href="#" className="flex flex-col justify-between px-7 py-8 bg-[#F0F4F8] rounded-2xl">
              <Image src="/images/icons/warning.png" width="14" height="14" alt="warning" />
              <div className="flex flex-col gap-2 mt-14">
                <span className="font-semibold text-2xl">@BOLT</span>
                <p className="text-lg">$TON @sniper bot</p>
                <p className="font-semibold text-lg text-primary flex gap-1 items-center">Explore options
                  <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M8 18L13 13.5L8 9" stroke="currentColor" strokeWidth="2.2" strokeLinecap="round" strokeLinejoin="round" />
                  </svg>
                </p>
              </div>
            </a>
            <a href="#" className="flex flex-col justify-between px-7 py-8 bg-[#F0F4F8] rounded-2xl">
              <Image src="/images/icons/warning.png" width="14" height="14" alt="warning" />
              <div className="flex flex-col gap-2 mt-14">
                <span className="font-semibold text-2xl">@MONETIZE</span>
                <p className="text-lg">@monetize any @crypto to $TON via Telegram Ads</p>
                <p className="font-semibold text-lg text-primary flex gap-1 items-center">Explore options
                  <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M8 18L13 13.5L8 9" stroke="currentColor" strokeWidth="2.2" strokeLinecap="round" strokeLinejoin="round" />
                  </svg>
                </p>
              </div>
            </a>
            <a href="#" className="flex flex-col justify-between px-7 py-8 bg-[#F0F4F8] rounded-2xl">
              <Image src="/images/icons/warning.png" width="14" height="14" alt="warning" />
              <div className="flex flex-col gap-2 mt-14">
                <span className="font-semibold text-2xl">@DECENTRALIZED</span>
                <p className="text-lg">$TON Dex</p>
                <p className="font-semibold text-lg text-primary flex gap-1 items-center">Explore options
                  <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M8 18L13 13.5L8 9" stroke="currentColor" strokeWidth="2.2" strokeLinecap="round" strokeLinejoin="round" />
                  </svg>
                </p>
              </div>
            </a>
            <a href="#" className="flex flex-col justify-between px-7 py-8 bg-[#F0F4F8] rounded-2xl">
              <Image src="/images/icons/warning.png" width="14" height="14" alt="warning" />
              <div className="flex flex-col gap-2 mt-14">
                <span className="font-semibold text-2xl">@CENTRALIZED</span>
                <p className="text-lg">$TON cex</p>
                <p className="font-semibold text-lg text-primary flex gap-1 items-center">Explore options
                  <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M8 18L13 13.5L8 9" stroke="currentColor" strokeWidth="2.2" strokeLinecap="round" strokeLinejoin="round" />
                  </svg>
                </p>
              </div>
            </a>
            <a href="#" className="flex flex-col justify-between px-7 py-8 bg-[#F0F4F8] rounded-2xl">
              <Image src="/images/icons/warning.png" width="14" height="14" alt="warning" />
              <div className="flex flex-col gap-2 mt-14">
                <span className="font-semibold text-2xl">@EDUCATION</span>
                <p className="text-lg">Courses on the fundamentals of @crypto</p>
                <p className="font-semibold text-lg text-primary flex gap-1 items-center">Explore options
                  <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M8 18L13 13.5L8 9" stroke="currentColor" strokeWidth="2.2" strokeLinecap="round" strokeLinejoin="round" />
                  </svg>
                </p>
              </div>
            </a>
            <a href="#" className="flex flex-col justify-between px-7 py-8 bg-[#F0F4F8] rounded-2xl">
              <Image src="/images/icons/warning.png" width="14" height="14" alt="warning" />
              <div className="flex flex-col gap-2 mt-14">
                <span className="font-semibold text-2xl">@TLDS</span>
                <p className="text-lg">.TG domains marketplace</p>
                <p className="font-semibold text-lg text-primary flex gap-1 items-center">Explore options
                  <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M8 18L13 13.5L8 9" stroke="currentColor" strokeWidth="2.2" strokeLinecap="round" strokeLinejoin="round" />
                  </svg>
                </p>
              </div>
            </a>
            <a href="#" className="flex flex-col justify-between px-7 py-8 bg-[#F0F4F8] rounded-2xl">
              <Image src="/images/icons/warning.png" width="14" height="14" alt="warning" />
              <div className="flex flex-col gap-2 mt-14">
                <span className="font-semibold text-2xl">@WAGER BOT</span>
                <p className="text-lg">In channel $TON gambling and bot</p>
                <p className="font-semibold text-lg text-primary flex gap-1 items-center">Explore options
                  <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M8 18L13 13.5L8 9" stroke="currentColor" strokeWidth="2.2" strokeLinecap="round" strokeLinejoin="round" />
                  </svg>
                </p>
              </div>
            </a>
            <a href="#" className="flex flex-col justify-between px-7 py-8 bg-[#F0F4F8] rounded-2xl">
              <Image src="/images/icons/warning.png" width="14" height="14" alt="warning" />
              <div className="flex flex-col gap-2 mt-14">
                <span className="font-semibold text-2xl">@ALTS BOT</span>
                <p className="text-lg">Bot for creating Telegram alt accounts</p>
                <p className="font-semibold text-lg text-primary flex gap-1 items-center">Explore options
                  <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M8 18L13 13.5L8 9" stroke="currentColor" strokeWidth="2.2" strokeLinecap="round" strokeLinejoin="round" />
                  </svg>
                </p>
              </div>
            </a>
            <a href="#" className="flex flex-col justify-between px-7 py-8 bg-[#F0F4F8] rounded-2xl">
              <Image src="/images/icons/warning.png" width="14" height="14" alt="warning" />
              <div className="flex flex-col gap-2 mt-14">
                <span className="font-semibold text-2xl">@ALARM BOT</span>
                <p className="text-lg">.Real time @alarm on $TON @currencies</p>
                <p className="font-semibold text-lg text-primary flex gap-1 items-center">Explore options
                  <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M8 18L13 13.5L8 9" stroke="currentColor" strokeWidth="2.2" strokeLinecap="round" strokeLinejoin="round" />
                  </svg>
                </p>
              </div>
            </a>
            <a href="#" className="flex flex-col justify-between px-7 py-8 bg-[#F0F4F8] rounded-2xl">
              <Image src="/images/icons/warning.png" width="14" height="14" alt="warning" />
              <div className="flex flex-col gap-2 mt-14">
                <span className="font-semibold text-2xl">@TREND BOT</span>
                <p className="text-lg">.$TON @trend BOT</p>
                <p className="font-semibold text-lg text-primary flex gap-1 items-center">Explore options
                  <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M8 18L13 13.5L8 9" stroke="currentColor" strokeWidth="2.2" strokeLinecap="round" strokeLinejoin="round" />
                  </svg>
                </p>
              </div>
            </a>
            <a href="#" className="flex flex-col justify-between px-7 py-8 bg-[#F0F4F8] rounded-2xl">
              <Image src="/images/icons/warning.png" width="14" height="14" alt="warning" />
              <div className="flex flex-col gap-2 mt-14">
                <span className="font-semibold text-2xl">@LOOT</span>
                <p className="text-lg">.Lootbox in @games $TON currency</p>
                <p className="font-semibold text-lg text-primary flex gap-1 items-center">Explore options
                  <svg width={24} height={24} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M8 18L13 13.5L8 9" stroke="currentColor" strokeWidth="2.2" strokeLinecap="round" strokeLinejoin="round" />
                  </svg>
                </p>
              </div>
            </a>
          </div>
        </div>
      </section>
      <Footer />
    </main>
  );
}
