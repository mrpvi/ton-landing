import NewsBar from '@/components/NewsBar';
import Header from '@/components/Header';
import Hero from '@/components/Hero';
import GettingStart from '@/components/Sections/GettingStart';
import Info from '@/components/Sections/Info';
import Unlucking from '@/components/Sections/Unlucking';
import ExploreApps from '@/components/Sections/ExploreApps';
import FutureOfInterner from '@/components/Sections/FutureOfInterner';
import Footer from '@/components/Footer';

export default function Home() {
  return (
    <main className="flex min-h-screen flex-col">
     <NewsBar />
     <Header />
     <Hero />
     <GettingStart />
     <Info />
     <Unlucking />
     <ExploreApps />
     <Footer />
     {/* <FutureOfInterner /> */}
    </main>
  );
}
