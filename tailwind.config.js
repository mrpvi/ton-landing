/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    colors: {
      "current": 'currentColor',
      "transparent": 'transparent',
      "white": '#FFF',
      "black": '#000',
      'primary': '#458CFE',
      'secondary': '#7DD8FD',
      
      'button-secondary-stroke-light': '#e9eef1',

      'background-title-light': '#f7f9fb',
    },
    container: {
      center: true,
      padding: {
        DEFAULT: "1rem",
        md: "0px"
      },
    },
    screens: {
      sm: "640px",
      md: "734px",
      lg: "936px",
      xl: "1120px",
    },
    extend: {
      backgroundImage: {
        "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
        "gradient-conic":
          "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
      },
    },
    backgroundImage: {
      'news-gradient': 'linear-gradient(90.41deg,#458CFE -.89%,#7DD8FD 112.62%)',
      'gradient-1': 'linear-gradient(0deg, #79C9FF 0.12%, #1889FF 99.89%)',
      'gradient-2': 'linear-gradient(0deg, #79C9FF 0.12%, #007DFF 99.89%)',
      'gradient-3': 'linear-gradient(80deg, #458CFE 0%, #7DD8FD 100%)',
      'gradient-4': 'linear-gradient(80deg, #6E78FF 0%, #96A6FF 100%)',
    }
  },
  plugins: [],
};
